// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        form.classList.add('was-validated')
        event.preventDefault()
        event.stopPropagation()
        if (form.checkValidity() === true) {
          submitForm(form)
        } 
      }, false)
    })
  }, false)
}())

function submitForm(form) {
  let url = form.getAttribute('action')
  let $form = $(form)
  let formData = $form.serialize();

  form.classList.add('processing')

  $.ajax({
    method: "POST",
    url: url,
    data: formData
  })
    .done(function(data) {
      console.log('ajax done: ', data)
      form.classList.remove('processing', 'was-validated')
      form.reset()
    });

}