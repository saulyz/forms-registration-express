const path = require('path')
const express = require('express')
const app = express()
app.use(express.static('public'))
const port = 3000

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'index.html')))
app.post('/register', (req, res) => {
  console.log(req.body)
  
  setTimeout(() => {
    res.json({
      status: 200,
      message: 'got post data'
    })
  }, 2000)
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))